# React example with static pages

## Prerequisites
* Flow and Prettier installed globally:
```sh
npm install -g flow-bin prettier
```
* Editor with Flow and prettier support

## Fetch, install dependencies, and run
```sh
git clone https://gitlab.com/dilawarm/react-static-pages.git
cd react-static-pages
npm install
npm start
```
